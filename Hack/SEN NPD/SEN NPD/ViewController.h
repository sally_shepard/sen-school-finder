//
//  ViewController.h
//  SEN NPD
//
//  Created by Sally on 17/11/2012.
//  Copyright (c) 2012 lines of cocoa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SchoolListViewController;

@interface ViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@property (weak, nonatomic) IBOutlet UITextField *postcodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;

@property (strong, nonatomic) SchoolListViewController *schoolListVC;

- (IBAction)schoolActionPressed:(id)sender;
- (IBAction)schoolActionPlusPressed:(id)sender;
- (IBAction)statementPressed:(id)sender;

@end