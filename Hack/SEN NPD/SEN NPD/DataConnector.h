//
//  DataConnector.h
//  DataConnectionTest
//
//  Created by Rob Stearn on 18/11/2012.
//  Copyright (c) 2012 Cocoadelica. All rights reserved.
//
// URL FORMAT: [NSURL URLWithString:[NSString stringWithFormat:@"%@/app_service.php?qtype=%i&thing=%@&otherthing=%@", qtype, thing, otherthing]];
// You can keep adding more &thing=%@ 's to the end for as many paramters as we need per query.


#import <Foundation/Foundation.h>

@interface DataConnector : NSObject <NSURLConnectionDataDelegate>

@property (nonatomic, strong) NSMutableData *downloadData;
@property (nonatomic, strong) NSJSONSerialization *downloadJSON;

-(id)init;

-(void)initiateConnectionWithURL:(NSURL*)url;

@end
