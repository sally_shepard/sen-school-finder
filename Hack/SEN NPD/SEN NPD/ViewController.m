//
//  ViewController.m
//  SEN NPD
//
//  Created by Sally on 17/11/2012.
//  Copyright (c) 2012 lines of cocoa. All rights reserved.
//

#import "ViewController.h"
#import "SchoolListViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "DataConnector.h"

@interface ViewController ()

@end

@implementation ViewController

- (id)init{
    if (self == [super init]) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _plusButton.titleLabel.numberOfLines = 2;
    CAGradientLayer *shadow = [CAGradientLayer layer];
    shadow.frame = CGRectMake(0, 0, 320, 568);
    shadow.startPoint = CGPointMake(0.0, 1.0);
    shadow.endPoint = CGPointMake(0.0, 0.0);
    shadow.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:0.102 green:0.114 blue:0.106 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:0.102 green:0.114 blue:0.106 alpha:0.7] CGColor], nil];
    [_backgroundView.layer addSublayer:shadow];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    _plusButton.titleLabel.numberOfLines = 2;
    _actionButton.titleLabel.numberOfLines = 2;
    _plusButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    _actionButton.titleLabel.textAlignment = NSTextAlignmentCenter;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showSchoolList:(NSString*)senType {
    _schoolListVC = [[SchoolListViewController alloc] init];
    _schoolListVC.senType = senType;
    [self.navigationController pushViewController:_schoolListVC animated:YES];
     
     DataConnector *dc = [[DataConnector alloc] init];
     NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/app_service.php?qtype=%i", @"http://sen-npd.dfeappathon.rewiredstate.org", 0]];
     [dc initiateConnectionWithURL:url];
 
    
}

- (IBAction)schoolActionPressed:(id)sender {
    [self showSchoolList:@"A"];
}

- (IBAction)schoolActionPlusPressed:(id)sender {
    [self showSchoolList:@"P"];
}

- (IBAction)statementPressed:(id)sender {
    [self showSchoolList:@"S"];
}

@end