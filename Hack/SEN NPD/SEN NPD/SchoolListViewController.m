//
//  SchoolListViewController.m
//  SEN NPD
//
//  Created by Sally on 17/11/2012.
//  Copyright (c) 2012 lines of cocoa. All rights reserved.
//

#import "SchoolListViewController.h"
#import "DetailViewController.h"

@interface SchoolListViewController ()

@end

@implementation SchoolListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIImageView *buttonIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchbutton.png"]];
    UIBarButtonItem *filterButton = [[UIBarButtonItem alloc] initWithCustomView:buttonIV];
    self.navigationItem.rightBarButtonItem = filterButton;
    _schoolArray = @[@"St Matthew Academy", @"Kidbrooke School", @"The John Roan School", @"Thomas Tallis School", @"Blackheath High School"];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// table view methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_schoolArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.textLabel.text = [_schoolArray objectAtIndex:indexPath.row];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _detailVC = [[DetailViewController alloc] init];
    
    _detailVC.senType = _senType;
    _detailVC.schoolName = [_schoolArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:_detailVC animated:YES];
}


@end