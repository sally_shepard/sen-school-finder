//
//  DetailViewController.h
//  SEN NPD
//
//  Created by Sally on 17/11/2012.
//  Copyright (c) 2012 lines of cocoa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *detailTableView;
@property (strong, nonatomic) NSString *senType;

@property (strong, nonatomic) NSString *schoolName;

@property (strong, nonatomic) NSDictionary *schoolsDict;

@end
