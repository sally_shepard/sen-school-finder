//
//  DataConnector.m
//  DataConnectionTest
//
//  Created by Rob Stearn on 18/11/2012.
//  Copyright (c) 2012 Cocoadelica. All rights reserved.
//

#import "DataConnector.h"

@implementation DataConnector

- (id)init
{
    self = [super init];
    if (self) {
      _downloadData = [[NSMutableData alloc] init];
    }
    return self;
}

-(void)initiateConnectionWithURL:(NSURL*)url {
  if (_downloadData != nil) {
    _downloadData = nil;
    _downloadData = [[NSMutableData alloc] init];
  }
  NSURLRequest *request = [NSURLRequest requestWithURL:url];
  NSURLConnection* connection = [NSURLConnection connectionWithRequest:request delegate:self];
  [connection start];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
  NSLog(@"Connection failed with error: %@", error);
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSHTTPURLResponse *)response {
  NSLog(@"Response: %d: %@", [response statusCode], [NSHTTPURLResponse localizedStringForStatusCode:[response statusCode]]);
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
  [_downloadData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
  if (_downloadData) {
    _downloadJSON = [NSJSONSerialization JSONObjectWithData:_downloadData options:NSJSONWritingPrettyPrinted error:nil];
    NSLog(@"JSON: %@", _downloadJSON);
    //do something with the JSON object (it's made of arrays and dictionarys) here.
  } else {
    NSLog(@"No data returned");
  }
  
}


@end
