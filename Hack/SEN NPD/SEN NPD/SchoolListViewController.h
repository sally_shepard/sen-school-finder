//
//  SchoolListViewController.h
//  SEN NPD
//
//  Created by Sally on 17/11/2012.
//  Copyright (c) 2012 lines of cocoa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface SchoolListViewController : UIViewController

@property (strong, nonatomic) NSArray *dataArray;

@property (strong, nonatomic) NSArray *schoolArray;

@property (weak, nonatomic) IBOutlet UITableView *schoolTableView;

@property (strong, nonatomic) DetailViewController *detailVC;
@property (strong, nonatomic) NSString *senType;

@end