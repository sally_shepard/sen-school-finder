//
//  DetailViewController.m
//  SEN NPD
//
//  Created by Sally on 17/11/2012.
//  Copyright (c) 2012 lines of cocoa. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
//    NSArray *StMatthewAcademyArray = @[];
//    NSArray *KidbrookeSchoolArray = @[];
//    NSArray *TheJohnRoanSchoolArray = @[];
//    NSArray *ThomasTallisSchoolArray = @[];
//    NSArray *BlackheathHighSchoolArray = @[];
//    
//    _schoolsDict = @{@"St Matthew Academy" : StMatthewAcademyArray,
//                     @"Kidbrooke School" : KidbrookeSchoolArray,
//                     @"The John Roan School" : TheJohnRoanSchoolArray,
//                     @"Thomas Tallis School" : ThomasTallisSchoolArray,
//                     @"Blackheath High School" : BlackheathHighSchoolArray
//    };
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 14;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 60;
    }
    else if (indexPath.row == 2) {
        return 5; //for address
    }
    else if (indexPath.row == 3) {
        return 60;
    }
    return 35;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
    
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {
        cell.textLabel.text = _schoolName;
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.numberOfLines = 2;
    }
    else if (indexPath.row == 1) {
        cell.textLabel.text = @"";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont systemFontOfSize:16];
    }
    else if (indexPath.row == 2) {
        cell.textLabel.text = @"";
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.numberOfLines = 4;
    }
    else {
        UILabel *schoolResultLabel = [[UILabel alloc] initWithFrame:CGRectMake(130, 0, 80, 40)];
        schoolResultLabel.textColor = [UIColor darkGrayColor];
        schoolResultLabel.backgroundColor = [UIColor clearColor];
        
        UILabel *avgResultLabel = [[UILabel alloc] initWithFrame:CGRectMake(230, 0, 80, 40)];
        avgResultLabel.textColor = [UIColor darkGrayColor];
        avgResultLabel.backgroundColor = [UIColor clearColor];
        if (indexPath.row == 3) {
            cell.textLabel.text = @"Grade";
            schoolResultLabel.text = @"School Avg";
            schoolResultLabel.numberOfLines = 2;
            schoolResultLabel.font = [UIFont boldSystemFontOfSize:18];
            schoolResultLabel.textColor = [UIColor blackColor];
            avgResultLabel.text = @"National Avg";
            avgResultLabel.numberOfLines = 2;
            avgResultLabel.font = [UIFont boldSystemFontOfSize:18];
            avgResultLabel.textColor = [UIColor blackColor];
        }
        else if (indexPath.row == 4) {
            cell.textLabel.text = @"A*";
            schoolResultLabel.text = @"0.2304";
            if ([_senType isEqualToString:@"N"]) {
                avgResultLabel.text = @"3.2502";
                schoolResultLabel.text = @"3.878"; 
            }
            else if ([_senType isEqualToString:@"A"]) {
                avgResultLabel.text = @"0.1018";
                schoolResultLabel.text = @"0.0000"; 
            }
            else if ([_senType isEqualToString:@"P"]) {
                avgResultLabel.text = @"0.0720";
                schoolResultLabel.text = @"0.000"; 
            }
            else if ([_senType isEqualToString:@"S"]) {
                avgResultLabel.text = @"0.0440";
                schoolResultLabel.text = @"0.000"; 
            }
        }
        else if (indexPath.row == 5) {
            cell.textLabel.text = @"A";
            schoolResultLabel.text = @"0.2304";
            
            if ([_senType isEqualToString:@"N"]) {
                avgResultLabel.text = @"3.9188";
                schoolResultLabel.text = @"2.5"; 
            }
            else if ([_senType isEqualToString:@"A"]) {
                avgResultLabel.text = @"0.3071";
                schoolResultLabel.text = @"0.333"; 
            }
            else if ([_senType isEqualToString:@"P"]) {
                avgResultLabel.text = @"0.2164";
                schoolResultLabel.text = @"0.333"; 
            }
            else if ([_senType isEqualToString:@"S"]) {
                avgResultLabel.text = @"0.1218";
                schoolResultLabel.text = @"0.000"; 
            }

        }
        else if (indexPath.row == 6) {
            cell.textLabel.text = @"B";
            schoolResultLabel.text = @"0.2304";
            
            if ([_senType isEqualToString:@"N"]) {
                avgResultLabel.text = @"3.9314";
                schoolResultLabel.text = @"3.8677"; 
            }
            else if ([_senType isEqualToString:@"A"]) {
                avgResultLabel.text = @"0.7032";
                schoolResultLabel.text = @"0.394"; 
            }
            else if ([_senType isEqualToString:@"P"]) {
                avgResultLabel.text = @"0.4950";
                schoolResultLabel.text = @"0.533"; 
            }
            else if ([_senType isEqualToString:@"S"]) {
                avgResultLabel.text = @"0.2430";
                schoolResultLabel.text = @"0.1002"; 
            }
        }
        else if (indexPath.row == 7) {
            cell.textLabel.text = @"C";
            schoolResultLabel.text = @"0.2304";
            
            if ([_senType isEqualToString:@"N"]) {
                avgResultLabel.text = @"3.2696";
                schoolResultLabel.text = @"3.788"; 
            }
            else if ([_senType isEqualToString:@"A"]) {
                avgResultLabel.text = @"1.4780";
                schoolResultLabel.text = @"1.1098"; 
            }
            else if ([_senType isEqualToString:@"P"]) {
                avgResultLabel.text = @"1.0421";
                schoolResultLabel.text = @"1.0799"; 
            }
            else if ([_senType isEqualToString:@"S"]) {
                avgResultLabel.text = @"0.5280";
                schoolResultLabel.text = @"0.8322"; 
            }
        }
        else if (indexPath.row == 8) {
            cell.textLabel.text = @"D";
            schoolResultLabel.text = @"0.2304";
            
            if ([_senType isEqualToString:@"N"]) {
                avgResultLabel.text = @"1.359";
                schoolResultLabel.text = @"0.2983"; 
            }
            else if ([_senType isEqualToString:@"A"]) {
                avgResultLabel.text = @"1.2570";
                schoolResultLabel.text = @"1.722"; 
            }
            else if ([_senType isEqualToString:@"P"]) {
                avgResultLabel.text = @"0.9616";
                schoolResultLabel.text = @"0.263"; 
            }
            else if ([_senType isEqualToString:@"S"]) {
                avgResultLabel.text = @"0.5381";
                schoolResultLabel.text = @"0.9897"; 
            }
        }
        else if (indexPath.row == 9) {
            cell.textLabel.text = @"E";
            schoolResultLabel.text = @"0.2304";
            
            if ([_senType isEqualToString:@"N"]) {
                avgResultLabel.text = @"0.5611";
                schoolResultLabel.text = @"0.9726"; 
            }
            else if ([_senType isEqualToString:@"A"]) {
                avgResultLabel.text = @"0.9553";
                schoolResultLabel.text = @"0.8272"; 
            }
            else if ([_senType isEqualToString:@"P"]) {
                avgResultLabel.text = @"0.8436";
                schoolResultLabel.text = @"0.2872"; 
            }
            else if ([_senType isEqualToString:@"S"]) {
                avgResultLabel.text = @"0.5732";
                schoolResultLabel.text = @"0.2378"; 
            }
        }
        else if (indexPath.row == 10) {
            cell.textLabel.text = @"F";
            schoolResultLabel.text = @"0.2304";
            
            if ([_senType isEqualToString:@"N"]) {
                avgResultLabel.text = @"0.2357";
                schoolResultLabel.text = @"0.2833"; 
            }
            else if ([_senType isEqualToString:@"A"]) {
                avgResultLabel.text = @"0.6193";
                schoolResultLabel.text = @"0.9883"; 
            }
            else if ([_senType isEqualToString:@"P"]) {
                avgResultLabel.text = @"0.6668";
                schoolResultLabel.text = @"0.6253"; 
            }
            else if ([_senType isEqualToString:@"S"]) {
                avgResultLabel.text = @"0.5079";
                schoolResultLabel.text = @"0.8796"; 
            }
        }
        else if (indexPath.row == 11) {
            cell.textLabel.text = @"G";
            schoolResultLabel.text = @"0.2304";
            
            if ([_senType isEqualToString:@"N"]) {
                avgResultLabel.text = @"0.0934";
                schoolResultLabel.text = @"0.0633"; 
            }
            else if ([_senType isEqualToString:@"A"]) {
                avgResultLabel.text = @"0.63078";
                schoolResultLabel.text = @"0.9976"; 
            }
            else if ([_senType isEqualToString:@"P"]) {
                avgResultLabel.text = @"0.3990";
                schoolResultLabel.text = @"0.6483"; 
            }
            else if ([_senType isEqualToString:@"S"]) {
                avgResultLabel.text = @"0.3184";
                schoolResultLabel.text = @"0.87378"; 
            }
        }
        else if (indexPath.row == 12) {
            cell.textLabel.text = @"U";
            schoolResultLabel.text = @"0.2304";
            
            if ([_senType isEqualToString:@"N"]) {
                avgResultLabel.text = @"0.0557";
                schoolResultLabel.text = @"0.0023"; 
            }
            else if ([_senType isEqualToString:@"A"]) {
                avgResultLabel.text = @"0.1445";
                schoolResultLabel.text = @"0.2873"; 
            }
            else if ([_senType isEqualToString:@"P"]) {
                avgResultLabel.text = @"0.2324";
                schoolResultLabel.text = @"0.4833"; 
            }
            else if ([_senType isEqualToString:@"S"]) {
                avgResultLabel.text = @"0.1424";
                schoolResultLabel.text = @"0.4749"; 
            }
        }
        
        [cell.contentView addSubview:schoolResultLabel];
        [cell.contentView addSubview:avgResultLabel];
    }
    if (indexPath.row == 13) {
        cell.textLabel.text = @"";
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
