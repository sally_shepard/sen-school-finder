//
//  AppDelegate.h
//  SEN NPD
//
//  Created by Sally on 17/11/2012.
//  Copyright (c) 2012 lines of cocoa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;
@property (strong, nonatomic) UINavigationController *navigationController;

@end
