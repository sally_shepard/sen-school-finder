//
//  FirstViewController.h
//  NPD
//
//  Created by Sally on 17/11/2012.
//  Copyright (c) 2012 lines of cocoa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MasterViewController;

@interface FirstViewController : UIViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) MasterViewController *schoolResultsVC;

// core data
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

// xib outlets
@property (weak, nonatomic) IBOutlet UITextField *postcodeTextField;


- (IBAction)schoolActionPressed:(id)sender;
- (IBAction)schoolActionPlusPressed:(id)sender;
- (IBAction)statementPressed:(id)sender;

@end
