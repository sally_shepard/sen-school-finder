//
//  School.h
//  NPD
//
//  Created by Sally on 17/11/2012.
//  Copyright (c) 2012 lines of cocoa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface School : NSManagedObject

@property (nonatomic) NSTimeInterval timeStamp;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * avgGrade;
@property (nonatomic, retain) NSString * avgSENGrade;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * senType;

@end
