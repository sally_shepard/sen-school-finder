//
//  FirstViewController.m
//  NPD
//
//  Created by Sally on 17/11/2012.
//  Copyright (c) 2012 lines of cocoa. All rights reserved.
//

#import "FirstViewController.h"
#import "MasterViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
         
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _schoolResultsVC = [[MasterViewController alloc] init];
    _schoolResultsVC.managedObjectContext = _managedObjectContext;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)schoolActionPressed:(id)sender {
    // get schools with that post code
    
    // calculate results for school action
    
    // sort schools by performance average
    
    [self.navigationController pushViewController:_schoolResultsVC animated:YES];
}

- (IBAction)schoolActionPlusPressed:(id)sender {
    // get schools with that post code
    
    // calculate results for school action plus
    
    // sort schools by performance average
    
    [self.navigationController pushViewController:_schoolResultsVC animated:YES];
}

- (IBAction)statementPressed:(id)sender {
    // get schools with that post code
    
    // calculate results for statement
    
    // sort schools by performance average
    
    [self.navigationController pushViewController:_schoolResultsVC animated:YES];
}

@end
