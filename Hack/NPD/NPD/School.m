//
//  School.m
//  NPD
//
//  Created by Sally on 17/11/2012.
//  Copyright (c) 2012 lines of cocoa. All rights reserved.
//

#import "School.h"


@implementation School

@dynamic timeStamp;
@dynamic name;
@dynamic type;
@dynamic avgGrade;
@dynamic avgSENGrade;
@dynamic address;
@dynamic senType;

@end
