//
//  ViewController.m
//  Flickr Search
//
//  Created by Sally Shepard on 21/10/2012.
//
//

#import "ViewController.h"
#import "CollectonCell.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"wood_finegrain_barE_flat-Layer0Image.jpg"]];
    self.sec1Array = [[NSArray alloc] init];
    self.sec1Array = @[@"2011-12-29 11.04.34", @"2012-01-03 20.18.42", @"2012-01-26 11.18.27", @"2012-02-02 15.29.21", @"2012-02-02 16.46.51", @"2012-02-06 19.45.45", @"2012-06-28 15.58.39", @"2012-07-05 21.40.57", @"2012-07-14 13.39.55", @"2012-07-15 10.38.27", @"2012-07-15 10.40.46 HDR", @"2012-07-21 13.40.32", @"2012-07-21 13.41.09", @"2012-07-22 13.08.22", @"2012-07-28 16.19.06", @"2012-08-11 10.59.34", @"2012-08-11 14.36.52", @"2012-08-26 13.59.43", @"2011-12-29 11.04.34", @"2012-01-03 20.18.42", @"2012-01-26 11.18.27", @"2012-02-02 15.29.21", @"2012-02-02 16.46.51", @"2012-02-06 19.45.45", @"2012-06-28 15.58.39", @"2012-07-05 21.40.57", @"2012-07-14 13.39.55", @"2012-07-15 10.38.27", @"2012-07-15 10.40.46 HDR", @"2012-07-21 13.40.32", @"2012-07-21 13.41.09", @"2012-07-22 13.08.22", @"2012-07-28 16.19.06", @"2012-08-11 10.59.34", @"2012-08-11 14.36.52", @"2012-08-26 13.59.43", @"2011-12-29 11.04.34", @"2012-01-03 20.18.42", @"2012-01-26 11.18.27", @"2012-02-02 15.29.21", @"2012-02-02 16.46.51", @"2012-02-06 19.45.45", @"2012-06-28 15.58.39", @"2012-07-05 21.40.57", @"2012-07-14 13.39.55", @"2012-07-15 10.38.27", @"2012-07-15 10.40.46 HDR", @"2012-07-21 13.40.32", @"2012-07-21 13.41.09", @"2012-07-22 13.08.22", @"2012-07-28 16.19.06", @"2012-08-11 10.59.34", @"2012-08-11 14.36.52", @"2012-08-26 13.59.43"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)shareButtonTapped:(id)sender {
    // TODO
}

#pragma mark - UICollectionView Datasource
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.sec1Array count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectonCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.photo = [NSString stringWithFormat:@"%@.jpg", [self.sec1Array objectAtIndex:indexPath.row]];
    return cell;
}

/*- (UICollectionReusableView *)collectionView:
 (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 return [[UICollectionReusableView alloc] init];
 }*/


#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: Select Item
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}

#pragma mark – UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    UIImage *photo = [UIImage imageNamed:@""];
    CGSize retval = photo.size.width > 0 ? photo.size : CGSizeMake(100, 100);
    retval.height += 35;
    retval.width += 35;
    return retval;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(50, 20, 50, 20);
}

@end
