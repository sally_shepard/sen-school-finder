//
//  ViewController.h
//  Flickr Search
//
//  Created by Sally Shepard on 21/10/2012.
//
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property(nonatomic, weak) IBOutlet UIToolbar *toolbar;
@property(nonatomic, weak) IBOutlet UIBarButtonItem *shareButton;
@property(nonatomic, weak) IBOutlet UITextField *textField;
@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property NSDictionary *picDictionary;
@property NSArray *sec1Array;
@property NSArray *sec2Array;

- (IBAction)shareButtonTapped:(id)sender;

@end