//
//  AppDelegate.h
//  Flickr Search
//
//  Created by Sally Shepard on 21/10/2012.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
