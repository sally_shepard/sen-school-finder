//
//  AppDelegate.h
//  kjhsg
//
//  Created by Sally on 27/10/2012.
//  Copyright (c) 2012 Sally. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
