//
//  DetailViewController.h
//  ighhj
//
//  Created by Sally on 28/10/2012.
//  Copyright (c) 2012 Sally. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
