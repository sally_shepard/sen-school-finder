//
//  AppDelegate.h
//  lksdjhd
//
//  Created by Sally on 27/10/2012.
//  Copyright (c) 2012 Sally. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
